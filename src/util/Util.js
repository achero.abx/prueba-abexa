// import {Draggable} from 'react-draggable';
import { toast } from 'react-toastify';
export const notify = (title, type, options = {}) => {
    toast[type](title, {
        position: options.position ?? "top-right",
        autoClose: options.autoClose ?? 2000,
        hideProgressBar: options.hideProgressBar ?? false,
        closeOnClick: options.closeOnClick ?? true,
        pauseOnHover: options.pauseOnHover ?? true,
        draggable: true,
        progress: undefined,
    });
}
// export const ModalForm = ({
//     isOpen,
//     closeModal,
//     title,
//     action,
//     width,
//     children,
//     textButtons = { confirm: 'Guardar', denied: 'Cancelar' },
//     validButton = { confirm: true , denied: true}
// }) => {
//     return(
//         <>
//             {isOpen ?
//             (
//                 <>
//                     <Draggable>
//                         <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-[1000] outline-none focus:outline-none" >
//                             <div className={`relative w-${width ? width : 'auto'} my-4 mx-auto max-w-[90%]`}>
//                                 <div className='w-full flex flex-col bg-white outline-none border-0'>
//                                     <strong>
//                                         <div className='bg-violet-950 flex items-start p-3 rounded-t-md'>
//                                             <h2 className='text-xl font-medium'>{title}</h2>
//                                             <button onClick={closeModal}><i className='fas fa-times'></i></button>
//                                         </div>
//                                     </strong>
//                                     <div className='p-4 flex bg-white'>
//                                         {children}
//                                     </div>
//                                     <div className="bg-white flex items-center justify-end p-4 rounded-b">
//                                         {
//                                             validButton.confirm ?
//                                                 <button
//                                                     className="min-w-[100px] bg-blue-500 hover:bg-blue-600 text-white active:bg-blue-600 font-bold capitalize text-sm px-4 py-2 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 text-xs"
//                                                     type="button"
//                                                     onClick={async ()=>{action();}}
//                                                 >
//                                                 </button>
//                                             : ''
//                                         }
//                                         {
//                                             validButton.denied ?
//                                                 <button
//                                                     className=" text-white-500 hover:bg-stone-600 rounded background-transparent font-bold capitalize px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 text-xs"
//                                                     type="button"
//                                                     onClick={closeModal}
//                                                 >
//                                                     {textButtons.denied}
//                                                 </button>
//                                             : ''
//                                         }
//                                     </div>
//                                 </div>
//                             </div>
//                         </div>
//                     </Draggable>
//                 </>
//             ) : null }
//         </>
//     );
// }

