import { useEffect } from "react";
import { useProductos } from "../hooks/useProductos";
import { ModalForm } from "../util/Modal";
import { useModal } from "../hooks/useModal";
import { useNavigate } from "react-router-dom";
export const Productos = () => {
    const {characters, listarProductos}  = useProductos();
    const [isOpenCarrito,openModalCarrito,closeModalCarrito] = useModal();
    const navigate = useNavigate();
    useEffect(() => {
        listarProductos();
    },[]);
    const abrirModal = () =>{
        openModalCarrito();
    }


    return(
        <>
            <div className='w-full p-7'>
                <div className='flex flex-wrap'>
                {
                    characters.map((data,index) => {
                        return(
                            <>
                            <div key={index++} className='flex-none w-1/3 p-3 text-center '>
                                <div className='border-2 p-4 rounded-md'>
                                    <div className='text-center p-1'>
                                        <label className=" text-xl font-bold text-gray-500">{data.name}</label>
                                    </div>
                                        <div className="w-full flex flex-col justify-center text-center">
                                            <img src={data.image}></img>
                                           <div className="p-2">
                                                <label className="text-gray-500 font-bold text-lg">S/{index+1}.00</label> 
                                            </div> 
                                        </div>
                                        <div className="p-2 w-full flex justify-center">
                                            <button className="hover:bg-violet-700 p-2 rounded-md w-[200px]" onClick={(e)=>{abrirModal()}}>Agregar</button>
                                        </div>
                                </div>
                            </div>
                            
                            </>
                        )
                    })
                }
                </div>
            </div>
            <ModalForm isOpen={isOpenCarrito} closeModal={closeModalCarrito} title="Añadir a tu carrito"
                validButton = {{confirm: true, denied: false, cancel:true}}>
            </ModalForm>
        </>
    )
}