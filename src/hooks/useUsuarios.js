import {useState} from "react"
import { notify } from "../util/Util"
export const useUsuarios = () => {
    const userDefault = {
        user:'',
        password:''
    }
    const [ user, setUsuario ] = useState(userDefault)
    const usuarios = {
        'dt0':
        [
            {user:'andicht01@gmail.com',password:'12345',type:'cliente'},
            {user:'user@gmail.com',password:'12345',type:'cliente'},
            {user:'usuario@gmail.com',password:'12345',type:'cliente'},
            {user:'andy@gmail.com',password:'123',type:'admin'},
            {user:'admin@gmail.com',password:'12345',type:'admin'}
        ]
    }
    const validacionCredencial = () => {
        let arrayUsuario = usuarios['dt0']
        let contador = 0
        let mensaje = '';
        let tipo = '';
        arrayUsuario.forEach((data,index) => {
            if((data.user).toUpperCase() == (user.user).toUpperCase() && (data.password).toUpperCase() == (user.password).toUpperCase()){
                contador = 0;
                return
            }else{
                if((data.user).toUpperCase() == (user.user).toUpperCase()){
                    mensaje = 'Usuario incorrecto';
                    tipo = 'warning';
                }else{
                    if(user.user == '' || user.password ==''){
                        mensaje = 'Los campos no deben estar vacíos';
                        tipo = 'error';
                    }else{
                        mensaje = 'Usuario no existe';
                        tipo = 'error';
                    }
                }
                contador++;
            }
        });
        if(contador == 0){
            notify('Bienvenido','success');
        }else{
            notify(mensaje,tipo);
        }
    }
    const editarValorUsuario = (key,value) =>{
        setUsuario(user => {
            return {
                ...user,
                [key]: value
            }
        });
    }
    return {user,editarValorUsuario,validacionCredencial}
}