import React, { useEffect, useState } from 'react';
import { ModalForm } from '../util/Util';
export const useProductos = ()=>{
    const [characters, setCharacters] = useState([]);
    const listarProductos = async () => {
        // URL de la API de Rick and Morty
        const apiUrl = 'https://rickandmortyapi.com/api/character';
        // Realizar la solicitud a la API utilizando fetch
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => {
                setCharacters(data.results);
            })
            .catch((error) => {
                console.error('Error fetching data:', error);
            });
     
    }
    return {characters , listarProductos}
}