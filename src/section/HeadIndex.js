import React, {useState} from "react";
import { Modal} from '@mui/material'
import { useUsuarios } from "../hooks/useUsuarios";
import { useNavigate } from "react-router-dom";
// import {makeStyles} from "@mui/material";

export const HeadIndex = ()=>{
    const {user,editarValorUsuario,validacionCredencial} = useUsuarios();
    const [open, setOpen] = useState(false);
    const navigate = useNavigate();
    const handleOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    const redireccionar = (numero) => {
        if (numero == 1){
            navigate('/')
        }else{
            navigate('/Carrito')
        }
    }
    return(
        <>
            <div className="bg-violet-400">
                <nav>
                    <div className="w-full px-4 lg:px-2">
                        <div className="w-ful flex items-center">
                            <div className="w-[15%] flex flex-col text-center items-center">
                                <label className="font-bold">EMPRESA</label>
                                <img  className="h-[40px] cursor-pointer" onClick={()=>redireccionar(1)}
                                    src="https://cdn-icons-png.flaticon.com/512/4845/4845256.png">
                                </img>
                            </div>
                            <div className="w-[85%] flex">
                                <div className="w-[50%] h-full"></div>
                                <div className="w-[50%] h-full flex items-center">
                                    <div className="h-full p-4 border-r-2 hover:cursor-pointer hover:text-violet-900" onClick={handleOpen   }>
                                        <p className="font-bold ">Inicia sesión</p>
                                    </div>
                                    <div className="p-2 hover:text-violet-900 cursor-pointer border-r-2" onClick={()=> redireccionar(2)}>
                                        <i className="fa fa-cart-plus text-3xl" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>

            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                className="modal-container"
            >
                <div className="bg-white w-[400px] rounded-lg flex  flex-col justify-center">
                    <div className="w-full p-2">
                        <div className="w-full">
                            <div className="flex justify-end">
                                <i className="fa fa-times cursor-pointer hover:text-violet-900"  title="Cerrar" aria-hidden="true" onClick={handleClose}></i>
                            </div>
                        </div>
                        <fieldset className="border-2 rounded-xl">
                            <legend className="flex justify-center p-1 text-center">
                                <img  className="h-[40px]"
                                    src="https://cdn-icons-png.flaticon.com/512/4845/4845256.png">
                                </img>
                                <p className="text-2xl">¡Bienvenido!</p>
                            </legend>
                            <div className="w-full p-4">
                                <div className="w-full">
                                    <div className="w-full text-left">
                                        <span>Correo electrónico</span>
                                    </div>
                                    <div className="w-full float-left py-4">
                                        <input className="border-b-2 w-full focus:border-none p-2" onChange={(e)=> editarValorUsuario('user',e.target.value)}/>
                                    </div>
                                </div>
                                <div className="py-2">
                                    <div className="w-full text-left">
                                        <span>Contraseña</span>
                                    </div>
                                    <div className="w-full flex float-left py-4">
                                        <input type="password" className="border-b-2 w-[90%] focus:border-none p-2" onChange={(e)=> editarValorUsuario('password',e.target.value)}/>
                                        <div className="w-[10%] px-2">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-full text-center">
                                    <button className="w-[80px] bg-violet-500 rounded-lg p-2 text-white hover:bg-violet-800" onClick={()=> validacionCredencial()}>Ingresar</button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </Modal>
        </>
    )
}