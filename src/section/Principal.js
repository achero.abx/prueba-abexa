import React from "react";
import { HeadIndex } from "./HeadIndex";
export const Principal = ({children})=>{
    return(
        <>
            <div className="dark:text-white">
                <HeadIndex></HeadIndex>
                <div className="main overflow-auto">
                    <div className="w-[95%] mx-auto h-full pt-2 pb-4 relative">
                       {children}
                    </div>
                </div>
            </div>
        </>
    )
}