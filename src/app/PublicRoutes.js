import React from "react";
import {Routes,Route} from "react-router-dom";
import { Principal } from "../section/Principal";
import { AdministradorMode } from "../pages/AdministradorMode";
import { Productos } from "../pages/Productos";
import { Carrito } from "../pages/Carrito";
export const PublicRoutes = () =>{
    return(
        <>
        <Principal>
            <Routes>
                <Route path="/" element={<Productos/>}></Route>
                <Route path="/Carrito" element={<Carrito/>}></Route>
                <Route path="/Administrador" element={<AdministradorMode/>}></Route>
            </Routes>
        </Principal>
        </> 
    )
}