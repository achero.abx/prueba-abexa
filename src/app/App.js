// import { BrowserRouter } from 'react-router-dom'
import { PublicRoutes } from "./PublicRoutes";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { BrowserRouter } from 'react-router-dom'
import '../assets/App.css'
export const App = () =>{
  return (
    <>
      <BrowserRouter>
        <PublicRoutes></PublicRoutes>
        <ToastContainer></ToastContainer>
      </BrowserRouter>
    </>
  )
}
// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
